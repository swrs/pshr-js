# pshr-js
 
*pshr-js* is a package of JS and styles for the *Ruby on Rails* engine [*Pshr*](https://gitlab.com/swrs/pshr).

## Installation

Add *pshr-js* to dependencies:

```bash
$ yarn add https://gitlab.com/swrs/pshr-js.git
```

Import and start in your application:

```js
// e.g. application.js or admin.js
import Pshr from "@swrs/pshr-js"
import "@swrs/pshr-js/dist/index.css"

const pshr = new Pshr()
pshr.start()
```

## Development

To develop *pshr-js* while running *Rails*'s *Webpacker* it might be helpful to link to the local repository of *pshr-js* and watch for changes in your application's `node_modules`:

```bash
yarn add link:/path/to/pshr-js
```

```js
// config/webpack/development.js
process.env.NODE_ENV = process.env.NODE_ENV || 'development'
const environment = require('./environment')
environment.config.set("devServer.watchOptions.ignored", "/node_modules\/(?!@swrs).*/")

module.exports = environment.toWebpackConfig()
```
