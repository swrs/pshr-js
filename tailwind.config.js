module.exports = {
  prefix: "pshr-",
  corePlugins: [],
  theme: {
    screens: {
      sm: "376px",
      md: "1024px",
      lg: "1440px"
    }
  }
}
