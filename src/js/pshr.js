import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"

class Pshr {

  start() {
    console.log("start pshr")

    const application = Application.start()
    const context = require.context("./controllers", true, /\.js$/)
    application.load(definitionsFromContext(context))
  }
}

export default Pshr
