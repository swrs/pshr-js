import { Controller } from "stimulus"
import Strings from "@supercharge/strings"
import Uppy from "@uppy/core"
import XhrUpload from "@uppy/xhr-upload"
import Dashboard from "@uppy/dashboard"
import "@uppy/core/dist/style.css"
import "@uppy/dashboard/dist/style.css"

export default class extends Controller {

  static targets = ["input", "button"]

  connect() {
    const fileInput = this.element.querySelector("input[type='file']")
    if (fileInput) { fileInput.parentNode.removeChild(fileInput) }

    if (!this.hasButtonTarget) {
      let button = document.createElement("div")
      button.setAttribute("data-target", "pshr-file.button")
      button.classList.add("__button")
      button.innerHTML = "Select file…"
      this.element.append(button)
    }

    const buttonId = "a" + Strings.random().replace(/-/g, "")
    this.buttonTarget.setAttribute("id", buttonId)

    this.uppy = new Uppy({
      autoProceed: true,
      restrictions: {
        allowedFileTypes: this.mimeTypes,
        maxFileSize: this.maxFileSize,
        maxNumberOfFiles: 1
      }
    })
      .use(XhrUpload, {
        endpoint: this.endpoint
      })
      .use(Dashboard, {
        trigger: `#${buttonId}`
      })

    this.uppy.on("upload-success", (file, response) => { this.uploadSuccess(file, response) })
    this.uppy.on("complete", (result) => { this.uploadComplete(result) })
  }

  uploadSuccess(file, response) {
    this.dispatchEvent("uploadsuccess", { file: file, response: response }) 

    this.buttonTarget.innerHTML = file.name

    console.log(response.body, JSON.stringify(response.body))
    const data = JSON.stringify(response.body)
    this.inputTarget.value = data
  }

  uploadComplete(result) {
    this.dispatchEvent("uploadcomplete", { result: result }) 

    this.uppy.reset()

    const dashboard = this.uppy.getPlugin("Dashboard")
    if (dashboard.isModalOpen()) { dashboard.closeModal() }
  }

  dispatchEvent(name, detail = {}) {
    console.log(name, detail)

    const event = new CustomEvent(name, { detail: detail })
    this.element.dispatchEvent(event)
  }

  get endpoint() {
    return this.data.get("endpoint")
  }

  // sets valid mime-types for uppy instance using a comma-seperated list
  // eg.  data-pshr-file-mime-types="image/*" => allow all images
  //      data-pshr-file-mime-types="image/jpg,application/pdf" => allow .jpg and .pdf
  // no or empty value allows all mime-types
  get mimeTypes() {
    const mimeTypes = this.data.get("mimeTypes")
    return mimeTypes ? mimeTypes.split(",").map(v => v.trim()) : null
  }

  // sets max filesize for uppy instance in bytes
  // eg.  data-pshr-file-max-file-size="5242880" => max filesize is 5 MB (5 * 1024 * 1024)
  get maxFileSize() {
    const maxFileSize = this.data.get("maxFileSize")
    return maxFileSize ? parseInt(maxFileSize) : null
  }
}
